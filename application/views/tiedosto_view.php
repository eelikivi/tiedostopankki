<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h3>Lisää tiedosto</h3>
<?php echo $error; ?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('tiedosto/tallenna'); ?>
    <div class="col-sm-6 col-lg-4">        
        <div class="form-group row">
            <label class="col-sm-12">Nimi: <input class="form-control" name="nimi"></label>            
        </div> 
        <div class="form-group row">
            <label class="control-label col-sm-12">Tiedosto: <input class="form-control" type="file" name="tiedosto"></label>
        </div> 
        <div class="form-group row">
            <label class="col-sm-12">Kuvaus: <textarea class="form-control" rows="4" name="kuvaus"></textarea></label>
        </div>  
        <div class="form-group row">
            <div class="col-sm-6">
                <button class="btn btn-info">Tallenna</button>
                <a class="btn btn-default" href="<?php echo site_url(); ?>">Takaisin</a>
            </div>
        </div>
    </div>
</form>

    