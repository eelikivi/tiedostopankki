<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="<?php echo base_url() . "css/bootstrap.min.css"; ?>" rel="stylesheet">
        <link href="<?php echo base_url() . "css/bootstrap-theme.min.css"; ?>" rel="stylesheet">
        <link href="<?php echo base_url() . "css/style.css"; ?>" rel="stylesheet">
    </head>
    <body>
        <h1>Tiedostopankki</h1>
        <article>
            <?php
            $this->load->view($main_content);
            ?>
        </article>
        <script src="<?php echo base_url() . "js/bootstrap.js"; ?>"></script>
    </body>    
</html>