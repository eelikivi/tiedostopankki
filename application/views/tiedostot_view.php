<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-sm-12">
        <h3>Tiedostot</h3>
        <p><a class="btn btn-info pull-right" href="<?php echo site_url() . 'tiedosto/lisaa'; ?>">Lisää tiedosto</a></p>
    </div>
</div>
<table class="table table-bordered">
    <tr class="active">
        <th>Nimi</th>
        <th>Tiedosto</th>
        <th>Kuvaus</th>
        <th>Tallennettu</th>
        <th>Poista</th>
    </tr>       

    <?php
    foreach ($tiedostot as $tiedosto) {
        echo "<tr>";
        echo "<td>" . anchor(site_url(). 'uploads/' . $tiedosto->tiedosto, $tiedosto->nimi) . "</td>\n";
        echo "<td>" . anchor(site_url(). 'uploads/' . $tiedosto->tiedosto, $tiedosto->tiedosto) . "</td>\n";
        echo "<td>" . $tiedosto->kuvaus . "</td>\n";
        echo "<td>" . $tiedosto->aika . "</td>\n";
        echo "<td>" . anchor("tiedosto/poista/$tiedosto->id","<i class=\"glyphicon glyphicon-trash\"></i>") . "</td>\n";
        echo "</tr>";
    }
    ?>
</table>
<div class="row">
    <div class="container">
<?php 
    echo $this->pagination->create_links();
?>
    </div>
</div>
