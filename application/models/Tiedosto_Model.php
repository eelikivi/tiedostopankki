<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {
    public function __construct() {
        parent::__construct();        
    }
    
    public function hae_kaikki($limit = null, $offset = null) {
        $this->db->limit($limit,$offset);               
        $query = $this->db->get('tiedosto');
        return $query->result();
    }
    
    public function poista($id) {
        $this->db->select('*');
        $this->db->where('id',$id);
        $this->db->from('tiedosto');
        $query = $this->db->get();
        
        $this->db->where('id',$id);
        $this->db->delete('tiedosto');
        
        $row = $query->row_array();
        return $row;
    }
    
    public function laske_tiedostot() {
        return $this->db->count_all_results("tiedosto");
    }
    
    public function tallenna($data) {
        $this->db->insert('tiedosto',$data);
        return $this->db->insert_id();        
    }
    
    public function lisaa($tiedosto) { 
        chmod($this->config->item('upload_path'),0777);
        
        $data = array(
            'nimi' => $this->input->post('nimi'),
            'kuvaus' => $this->input->post('kuvaus'),                        
            'tiedosto' => $tiedosto,            
        );
        
        $this->tiedosto_model->tallenna($data);
        redirect('tiedosto/index');
    }
}