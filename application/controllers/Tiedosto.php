<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('tiedosto_model');
        $this->load->library(array('pagination','form_validation'));
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        $tiedostoja_sivulla = 3;

        $data['tiedostot'] = $this->tiedosto_model->hae_kaikki($tiedostoja_sivulla, $this->uri->segment(3));

        $config['base_url'] = site_url('tiedosto/index');
        $config['total_rows'] = $this->tiedosto_model->laske_tiedostot();
        $config['per_page'] = $tiedostoja_sivulla;
        $config['uri_segment'] = 3;

        $this->pagination->initialize($config);
        $data['main_content'] = 'tiedostot_view';
        $this->load->view('template', $data);
    }

    public function lisaa() {
        $data['error'] = "";
        $data['main_content'] = 'tiedosto_view';
        $this->load->view('template', $data);
    }

    public function poista($id) {
        $tiedosto = $this->tiedosto_model->poista($id);
        $polku = $this->config->item('upload_path') . '/' . $tiedosto['tiedosto'];
        unlink($polku);
        redirect('tiedosto/index');
    }

    public function tallenna() {
        $this->form_validation->set_rules('nimi', 'nimi', 'required');
        
        if ($this->form_validation->run()) {
            
            $config['upload_path'] = 'uploads';
            $config['allowed_types'] = '*';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('tiedosto')) {
                $data['error'] = $this->upload->display_errors();
                $data['main_content'] = 'tiedosto_view';
                $this->load->view('template', $data);
            } 
            else {
                $tiedosto = $this->upload->data();
                $this->tiedosto_model->lisaa($tiedosto['file_name']);
            }
        }
        else {
            $data['error'] = "";
            $data['main_content'] = 'tiedosto_view';
            $this->load->view('template', $data);
        }
    }
}
