/**/
DROP DATABASE IF EXISTS tiedostopankki;
CREATE DATABASE tiedostopankki;

USE tiedostopankki;

CREATE TABLE tiedosto(
    id int primary key auto_increment,
    nimi varchar(30) not null,
    tiedosto varchar(256) not null,
    kuvaus varchar(256),
    aika timestamp default current_timestamp
    on update current_timestamp

);